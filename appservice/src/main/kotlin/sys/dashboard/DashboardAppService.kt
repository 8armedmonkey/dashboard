package sys.dashboard

class DashboardAppService(
    private val dashboardRepository: DashboardRepository,
    private val layoutRepository: LayoutRepository,
    private val widgetRepository: WidgetRepository,
    private val settingsFactory: SettingsFactory
) {

    fun retrieveDashboards(dashboardOwnerId: DashboardOwnerId): List<Dashboard> {
        return dashboardRepository.list(dashboardOwnerId)
    }

    fun retrieveDashboard(
        dashboardOwnerId: DashboardOwnerId,
        dashboardId: DashboardId
    ): Dashboard? {
        return dashboardRepository.retrieve(dashboardOwnerId, dashboardId)
    }

    fun setDashboardLayout(
        dashboardOwnerId: DashboardOwnerId,
        dashboardId: DashboardId,
        layoutId: LayoutId
    ) {
        val dashboard = retrieveAndAssertDashboardExists(dashboardOwnerId, dashboardId)
        val layout = retrieveAndAssertLayoutExists(layoutId)

        dashboard.setLayout(layout)

        dashboardRepository.store(dashboardOwnerId, dashboard)
    }

    fun addWidget(
        dashboardOwnerId: DashboardOwnerId,
        dashboardId: DashboardId,
        widgetId: WidgetId,
        areaId: AreaId
    ): WidgetAssignmentId {
        val dashboard = retrieveAndAssertDashboardExists(dashboardOwnerId, dashboardId)
        val widget = retrieveAndAssertWidgetExists(widgetId)
        val settings = settingsFactory.getDefaultSettings(widgetId)

        val widgetAssignmentId = dashboard.addWidget(widget, areaId, settings)
        dashboardRepository.store(dashboardOwnerId, dashboard)

        return widgetAssignmentId
    }

    fun configureWidget(
        dashboardOwnerId: DashboardOwnerId,
        dashboardId: DashboardId,
        widgetAssignmentId: WidgetAssignmentId,
        settings: Settings
    ) {
        val dashboard = retrieveAndAssertDashboardExists(dashboardOwnerId, dashboardId)

        dashboard.configureWidget(widgetAssignmentId, settings)
        dashboardRepository.store(dashboardOwnerId, dashboard)
    }

    fun increaseOrder(
        dashboardOwnerId: DashboardOwnerId,
        dashboardId: DashboardId,
        widgetAssignmentId: WidgetAssignmentId
    ) {
        val dashboard = retrieveAndAssertDashboardExists(dashboardOwnerId, dashboardId)

        dashboard.increaseOrder(widgetAssignmentId)
        dashboardRepository.store(dashboardOwnerId, dashboard)
    }

    fun decreaseOrder(
        dashboardOwnerId: DashboardOwnerId,
        dashboardId: DashboardId,
        widgetAssignmentId: WidgetAssignmentId
    ) {
        val dashboard = retrieveAndAssertDashboardExists(dashboardOwnerId, dashboardId)

        dashboard.decreaseOrder(widgetAssignmentId)
        dashboardRepository.store(dashboardOwnerId, dashboard)
    }

    fun moveBefore(
        dashboardOwnerId: DashboardOwnerId,
        dashboardId: DashboardId,
        toBeMoved: WidgetAssignmentId,
        before: WidgetAssignmentId
    ) {
        TODO()
    }

    fun moveAfter(
        dashboardOwnerId: DashboardOwnerId,
        dashboardId: DashboardId,
        toBeMoved: WidgetAssignmentId,
        after: WidgetAssignmentId
    ) {
        TODO()
    }

    fun removeWidget(
        dashboardOwnerId: DashboardOwnerId,
        dashboardId: DashboardId,
        widgetAssignmentId: WidgetAssignmentId
    ) {
        val dashboard = retrieveAndAssertDashboardExists(dashboardOwnerId, dashboardId)

        dashboard.removeWidget(widgetAssignmentId)
        dashboardRepository.store(dashboardOwnerId, dashboard)
    }

    fun clearWidgets(
        dashboardOwnerId: DashboardOwnerId,
        dashboardId: DashboardId
    ) {
        val dashboard = retrieveAndAssertDashboardExists(dashboardOwnerId, dashboardId)

        dashboard.clearWidgets()
        dashboardRepository.store(dashboardOwnerId, dashboard)
    }

    fun retrieveLayouts(): List<Layout> {
        return layoutRepository.list()
    }

    fun retrieveWidgets(): List<Widget> {
        return widgetRepository.list()
    }

    private fun retrieveAndAssertDashboardExists(
        dashboardOwnerId: DashboardOwnerId,
        dashboardId: DashboardId
    ): Dashboard {
        return dashboardRepository.retrieve(dashboardOwnerId, dashboardId)
            ?: throw IllegalArgumentException("Dashboard with the specified ID does not exist.")
    }

    private fun retrieveAndAssertLayoutExists(layoutId: LayoutId): Layout {
        return layoutRepository.retrieve(layoutId)
            ?: throw IllegalArgumentException("Layout with the specified ID does not exist.")
    }

    private fun retrieveAndAssertWidgetExists(widgetId: WidgetId): Widget {
        return widgetRepository.retrieve(widgetId)
            ?: throw IllegalArgumentException("Widget with the specified ID does not exist.")
    }

}