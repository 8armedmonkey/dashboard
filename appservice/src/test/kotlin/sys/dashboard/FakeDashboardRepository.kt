package sys.dashboard

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever

object FakeDashboardRepository {

    fun setUpRetrieveDashboard(dashboardRepository: DashboardRepository, dashboard: Dashboard) {
        whenever(dashboardRepository.retrieve(any(), any())).thenReturn(dashboard)
    }

}