package sys.dashboard

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever

object FakeSettingsFactory {

    fun setUpGetDefaultSettings(settingsFactory: SettingsFactory, settings: Settings) {
        whenever(settingsFactory.getDefaultSettings(any())).thenReturn(settings)
    }

}