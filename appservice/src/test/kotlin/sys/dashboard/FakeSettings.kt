package sys.dashboard

import com.nhaarman.mockitokotlin2.mock

object FakeSettings {

    fun newFakeSettings(): Settings = mock()

    fun newFakeBaseSettings(): BaseSettings = FakeBaseSettings()

}