package sys.dashboard

import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import sys.utils.toNameUUID

class DashboardAppServiceTest {

    private lateinit var dashboardRepository: DashboardRepository
    private lateinit var layoutRepository: LayoutRepository
    private lateinit var widgetRepository: WidgetRepository
    private lateinit var settingsFactory: SettingsFactory
    private lateinit var dashboardAppService: DashboardAppService

    @Before
    fun setUp() {
        dashboardRepository = mock()
        layoutRepository = mock()
        widgetRepository = mock()
        settingsFactory = mock()
        dashboardAppService = DashboardAppService(
            dashboardRepository = dashboardRepository,
            layoutRepository = layoutRepository,
            widgetRepository = widgetRepository,
            settingsFactory = settingsFactory
        )
    }

    @Test
    fun `when retrieving dashboards then it should request the repository with correct arguments`() {
        val dashboardOwnerId = DashboardOwnerId("dashboard-owner".toNameUUID())

        dashboardAppService.retrieveDashboards(dashboardOwnerId)

        verify(dashboardRepository).list(eq(dashboardOwnerId))
    }

    @Test
    fun `when retrieving dashboard then it should request the repository with correct arguments`() {
        val dashboardOwnerId = DashboardOwnerId("dashboard-owner".toNameUUID())
        val dashboardId = DashboardId("dashboard".toNameUUID())

        dashboardAppService.retrieveDashboard(dashboardOwnerId, dashboardId)

        verify(dashboardRepository).retrieve(eq(dashboardOwnerId), eq(dashboardId))
    }

    @Test
    fun `when setting dashboard layout then it should set the dashboard with the layout`() {
        val dashboardOwnerId = DashboardOwnerId("dashboard-owner".toNameUUID())
        val dashboard = Dashboard(
            id = DashboardId("dashboard".toNameUUID()),
            metaData = DashboardMetaData(
                name = "Test Dashboard",
                description = "Dashboard under test."
            )
        )
        val layout = Layout(
            id = LayoutId("layout".toNameUUID()),
            metaData = LayoutMetaData(
                name = "Test Layout",
                description = "Layout under test."
            ),
            areaIds = setOf(
                AreaId("left-column".toNameUUID()),
                AreaId("right-column".toNameUUID())
            )
        )

        FakeDashboardRepository.setUpRetrieveDashboard(dashboardRepository, dashboard)
        FakeLayoutRepository.setUpRetrieveLayout(layoutRepository, layout)

        dashboardAppService.setDashboardLayout(dashboardOwnerId, dashboard.id, layout.id)

        argumentCaptor<Dashboard>().apply {
            verify(dashboardRepository).store(eq(dashboardOwnerId), capture())

            assertEquals(layout.id, firstValue.getWidgetAssignments()?.layout?.id)
        }
    }

    @Test
    fun `when adding widget then it should add the widget to the dashboard and store it in the repository`() {
        val dashboardOwnerId = DashboardOwnerId("dashboard-owner".toNameUUID())
        val dashboard = Dashboard(
            id = DashboardId("dashboard".toNameUUID()),
            metaData = DashboardMetaData(
                name = "Test Dashboard",
                description = "Dashboard under test."
            )
        )
        val layout = Layout(
            id = LayoutId("layout".toNameUUID()),
            metaData = LayoutMetaData(
                name = "Test Layout",
                description = "Layout under test."
            ),
            areaIds = setOf(
                AreaId("left-column".toNameUUID()),
                AreaId("right-column".toNameUUID())
            )
        )
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = layout.areaIds.first()
        val settings = FakeSettings.newFakeSettings()

        dashboard.setLayout(layout)

        FakeDashboardRepository.setUpRetrieveDashboard(dashboardRepository, dashboard)
        FakeWidgetRepository.setUpRetrieveWidget(widgetRepository, widget)
        FakeSettingsFactory.setUpGetDefaultSettings(settingsFactory, settings)

        val widgetAssignmentId = dashboardAppService.addWidget(
            dashboardOwnerId,
            dashboard.id,
            widget.id,
            areaId
        )

        argumentCaptor<Dashboard>().apply {
            verify(dashboardRepository).store(eq(dashboardOwnerId), capture())

            val widgetAssignmentsByArea = firstValue.getWidgetAssignments()?.getByArea(areaId)
            val widgetAssignment = widgetAssignmentsByArea?.find { it.id == widgetAssignmentId }

            assertNotNull(widgetAssignment)
            assertEquals(widgetAssignmentId, widgetAssignment?.id)
            assertEquals(widget, widgetAssignment?.widget)
            assertEquals(settings, widgetAssignment?.settings)
        }
    }

    @Test
    fun `when configuring widget then it should set the new settings and store it in the repository`() {
        val dashboardOwnerId = DashboardOwnerId("dashboard-owner".toNameUUID())
        val dashboard = Dashboard(
            id = DashboardId("dashboard".toNameUUID()),
            metaData = DashboardMetaData(
                name = "Test Dashboard",
                description = "Dashboard under test."
            )
        )
        val layout = Layout(
            id = LayoutId("layout".toNameUUID()),
            metaData = LayoutMetaData(
                name = "Test Layout",
                description = "Layout under test."
            ),
            areaIds = setOf(
                AreaId("left-column".toNameUUID()),
                AreaId("right-column".toNameUUID())
            )
        )
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = layout.areaIds.first()
        val settings = FakeSettings.newFakeSettings()
        val newSettings = FakeSettings.newFakeSettings()

        dashboard.setLayout(layout)

        val widgetAssignmentId = dashboard.addWidget(widget, areaId, settings)

        FakeDashboardRepository.setUpRetrieveDashboard(dashboardRepository, dashboard)

        dashboardAppService.configureWidget(
            dashboardOwnerId,
            dashboard.id,
            widgetAssignmentId,
            newSettings
        )

        argumentCaptor<Dashboard>().apply {
            verify(dashboardRepository).store(eq(dashboardOwnerId), capture())

            val widgetAssignmentsByArea = firstValue.getWidgetAssignments()?.getByArea(areaId)
            val widgetAssignment = widgetAssignmentsByArea?.find { it.id == widgetAssignmentId }

            assertEquals(newSettings, widgetAssignment?.settings)
        }
    }

    @Test
    fun `when increasing widget's order then it should move the widget to later position and store it in the repository`() {
        val dashboardOwnerId = DashboardOwnerId("dashboard-owner".toNameUUID())
        val dashboard = Dashboard(
            id = DashboardId("dashboard".toNameUUID()),
            metaData = DashboardMetaData(
                name = "Test Dashboard",
                description = "Dashboard under test."
            )
        )
        val layout = Layout(
            id = LayoutId("layout".toNameUUID()),
            metaData = LayoutMetaData(
                name = "Test Layout",
                description = "Layout under test."
            ),
            areaIds = setOf(
                AreaId("left-column".toNameUUID()),
                AreaId("right-column".toNameUUID())
            )
        )
        val widget1 = Widget(
            id = WidgetId("widget-1".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 1",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val widget2 = Widget(
            id = WidgetId("widget-2".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 2",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget2"
        )
        val areaId = layout.areaIds.first()

        dashboard.setLayout(layout)

        val widgetAssignmentId1 = dashboard.addWidget(widget1, areaId, FakeSettings.newFakeSettings())
        val widgetAssignmentId2 = dashboard.addWidget(widget2, areaId, FakeSettings.newFakeSettings())

        FakeDashboardRepository.setUpRetrieveDashboard(dashboardRepository, dashboard)

        dashboardAppService.increaseOrder(dashboardOwnerId, dashboard.id, widgetAssignmentId1)

        argumentCaptor<Dashboard>().apply {
            verify(dashboardRepository).store(eq(dashboardOwnerId), capture())

            val widgetAssignmentsByArea = firstValue.getWidgetAssignments()?.getByArea(areaId)

            assertEquals(widgetAssignmentId2, widgetAssignmentsByArea?.get(0)?.id)
            assertEquals(widget2, widgetAssignmentsByArea?.get(0)?.widget)

            assertEquals(widgetAssignmentId1, widgetAssignmentsByArea?.get(1)?.id)
            assertEquals(widget1, widgetAssignmentsByArea?.get(1)?.widget)
        }
    }

    @Test
    fun `when decreasing widget's order then it should move the widget to earlier position and store it in the repository`() {
        val dashboardOwnerId = DashboardOwnerId("dashboard-owner".toNameUUID())
        val dashboard = Dashboard(
            id = DashboardId("dashboard".toNameUUID()),
            metaData = DashboardMetaData(
                name = "Test Dashboard",
                description = "Dashboard under test."
            )
        )
        val layout = Layout(
            id = LayoutId("layout".toNameUUID()),
            metaData = LayoutMetaData(
                name = "Test Layout",
                description = "Layout under test."
            ),
            areaIds = setOf(
                AreaId("left-column".toNameUUID()),
                AreaId("right-column".toNameUUID())
            )
        )
        val widget1 = Widget(
            id = WidgetId("widget-1".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 1",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val widget2 = Widget(
            id = WidgetId("widget-2".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 2",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget2"
        )
        val areaId = layout.areaIds.first()

        dashboard.setLayout(layout)

        val widgetAssignmentId1 = dashboard.addWidget(widget1, areaId, FakeSettings.newFakeSettings())
        val widgetAssignmentId2 = dashboard.addWidget(widget2, areaId, FakeSettings.newFakeSettings())

        FakeDashboardRepository.setUpRetrieveDashboard(dashboardRepository, dashboard)

        dashboardAppService.decreaseOrder(dashboardOwnerId, dashboard.id, widgetAssignmentId2)

        argumentCaptor<Dashboard>().apply {
            verify(dashboardRepository).store(eq(dashboardOwnerId), capture())

            val widgetAssignmentsByArea = firstValue.getWidgetAssignments()?.getByArea(areaId)

            assertEquals(widgetAssignmentId2, widgetAssignmentsByArea?.get(0)?.id)
            assertEquals(widget2, widgetAssignmentsByArea?.get(0)?.widget)

            assertEquals(widgetAssignmentId1, widgetAssignmentsByArea?.get(1)?.id)
            assertEquals(widget1, widgetAssignmentsByArea?.get(1)?.widget)
        }
    }

    @Test
    fun `when removing widget then it should remove the widget from the dashboard and store it in the repository`() {
        val dashboardOwnerId = DashboardOwnerId("dashboard-owner".toNameUUID())
        val dashboard = Dashboard(
            id = DashboardId("dashboard".toNameUUID()),
            metaData = DashboardMetaData(
                name = "Test Dashboard",
                description = "Dashboard under test."
            )
        )
        val layout = Layout(
            id = LayoutId("layout".toNameUUID()),
            metaData = LayoutMetaData(
                name = "Test Layout",
                description = "Layout under test."
            ),
            areaIds = setOf(
                AreaId("left-column".toNameUUID()),
                AreaId("right-column".toNameUUID())
            )
        )
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = layout.areaIds.first()
        val settings = FakeSettings.newFakeSettings()

        dashboard.setLayout(layout)

        val widgetAssignmentId = dashboard.addWidget(widget, areaId, settings)

        FakeDashboardRepository.setUpRetrieveDashboard(dashboardRepository, dashboard)

        dashboardAppService.removeWidget(dashboardOwnerId, dashboard.id, widgetAssignmentId)

        argumentCaptor<Dashboard>().apply {
            verify(dashboardRepository).store(eq(dashboardOwnerId), capture())

            val widgetAssignmentsByArea = firstValue.getWidgetAssignments()?.getByArea(areaId)
            val widgetAssignment = widgetAssignmentsByArea?.find { it.id == widgetAssignmentId }

            assertNull(widgetAssignment)
        }
    }

    @Test
    fun `when clearing widgets then it should remove all widgets from the dashboard and store it in the repository`() {
        val dashboardOwnerId = DashboardOwnerId("dashboard-owner".toNameUUID())
        val dashboard = Dashboard(
            id = DashboardId("dashboard".toNameUUID()),
            metaData = DashboardMetaData(
                name = "Test Dashboard",
                description = "Dashboard under test."
            )
        )
        val layout = Layout(
            id = LayoutId("layout".toNameUUID()),
            metaData = LayoutMetaData(
                name = "Test Layout",
                description = "Layout under test."
            ),
            areaIds = setOf(
                AreaId("left-column".toNameUUID()),
                AreaId("right-column".toNameUUID())
            )
        )
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = layout.areaIds.first()
        val settings = FakeSettings.newFakeSettings()

        dashboard.setLayout(layout)
        dashboard.addWidget(widget, areaId, settings)

        FakeDashboardRepository.setUpRetrieveDashboard(dashboardRepository, dashboard)

        dashboardAppService.clearWidgets(dashboardOwnerId, dashboard.id)

        argumentCaptor<Dashboard>().apply {
            verify(dashboardRepository).store(eq(dashboardOwnerId), capture())

            assertEquals(true, firstValue.getWidgetAssignments()?.isEmpty())
        }
    }

    @Test
    fun `when retrieving layouts then it should request the repository`() {
        FakeLayoutRepository.setUpListLayouts(layoutRepository, listOf())

        dashboardAppService.retrieveLayouts()

        verify(layoutRepository).list()
    }

    @Test
    fun `when retrieving widgets then it should request the repository`() {
        FakeWidgetRepository.setUpListWidgets(widgetRepository, listOf())

        dashboardAppService.retrieveWidgets()

        verify(widgetRepository).list()
    }

}