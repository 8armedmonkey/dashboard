package sys.dashboard

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever

object FakeWidgetRepository {

    fun setUpListWidgets(widgetRepository: WidgetRepository, widgets: List<Widget>) {
        whenever(widgetRepository.list()).thenReturn(widgets)
    }

    fun setUpRetrieveWidget(widgetRepository: WidgetRepository, widget: Widget) {
        whenever(widgetRepository.retrieve(any())).thenReturn(widget)
    }

}