package sys.dashboard

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever

object FakeLayoutRepository {

    fun setUpListLayouts(layoutRepository: LayoutRepository, layouts: List<Layout>) {
        whenever(layoutRepository.list()).thenReturn(layouts)
    }

    fun setUpRetrieveLayout(layoutRepository: LayoutRepository, layout: Layout) {
        whenever(layoutRepository.retrieve(any())).thenReturn(layout)
    }

}