package sys.utils

fun <T> MutableList<T>.swap(index: Int, otherIndex: Int) {
    if (index == otherIndex) return

    val item = get(index)
    val otherItem = get(otherIndex)

    set(index, otherItem)
    set(otherIndex, item)
}