package sys.utils

import java.util.*

fun String.toNameUUID(): UUID = UUID.nameUUIDFromBytes(toByteArray())