package sys.dashboard

import java.util.*

data class DashboardOwnerId(
    val rawValue: UUID = UUID.randomUUID()
)