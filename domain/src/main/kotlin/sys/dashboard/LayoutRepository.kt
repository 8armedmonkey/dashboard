package sys.dashboard

interface LayoutRepository {

    fun list(): List<Layout>

    fun store(layout: Layout)

    fun retrieve(layoutId: LayoutId): Layout?

    fun remove(layoutId: LayoutId)

    fun clear()

}