package sys.dashboard

data class WidgetAssignment(
    val id: WidgetAssignmentId,
    val widget: Widget,
    val settings: Settings
)