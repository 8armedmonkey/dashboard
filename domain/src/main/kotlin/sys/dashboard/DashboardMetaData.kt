package sys.dashboard

data class DashboardMetaData(
    val name: String,
    val description: String
)
