package sys.dashboard

class Dashboard(
    val id: DashboardId,
    val metaData: DashboardMetaData
) {

    private var widgetAssignments: WidgetAssignments? = null

    fun setLayout(layout: Layout) {
        widgetAssignments = WidgetAssignments(layout)
    }

    fun getWidgetAssignments(): WidgetAssignments? {
        return widgetAssignments
    }

    fun addWidget(widget: Widget, areaId: AreaId, settings: Settings): WidgetAssignmentId {
        return widgetAssignments?.add(widget, areaId, settings)
            ?: throw IllegalStateException("Layout has not been assigned.")
    }

    fun configureWidget(widgetAssignmentId: WidgetAssignmentId, settings: Settings) {
        widgetAssignments?.configure(widgetAssignmentId, settings)
            ?: throw IllegalStateException("Layout has not been assigned.")
    }

    fun increaseOrder(widgetAssignmentId: WidgetAssignmentId) {
        widgetAssignments?.increaseOrder(widgetAssignmentId)
            ?: throw IllegalStateException("Layout has not been assigned.")
    }

    fun decreaseOrder(widgetAssignmentId: WidgetAssignmentId) {
        widgetAssignments?.decreaseOrder(widgetAssignmentId)
            ?: throw IllegalStateException("Layout has not been assigned.")
    }

    fun moveBefore(
        toBeMoved: WidgetAssignmentId,
        before: WidgetAssignmentId
    ) {
        TODO()
    }

    fun moveAfter(
        toBeMoved: WidgetAssignmentId,
        after: WidgetAssignmentId
    ) {
        TODO()
    }

    fun removeWidget(widgetAssignmentId: WidgetAssignmentId) {
        widgetAssignments?.remove(widgetAssignmentId)
            ?: throw IllegalStateException("Layout has not been assigned.")
    }

    fun clearWidgets() {
        widgetAssignments?.clear()
            ?: throw IllegalStateException("Layout has not been assigned.")
    }

}