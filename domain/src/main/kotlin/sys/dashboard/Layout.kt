package sys.dashboard

data class Layout(
    val id: LayoutId,
    val metaData: LayoutMetaData,
    val areaIds: Set<AreaId>
) {

    fun hasArea(areaId: AreaId) = areaIds.contains(areaId)

}