package sys.dashboard

data class WidgetMetaData(
    val name: String,
    val description: String
)