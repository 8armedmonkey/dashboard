package sys.dashboard

import java.util.*

data class DashboardId(
    val rawValue: UUID = UUID.randomUUID()
)
