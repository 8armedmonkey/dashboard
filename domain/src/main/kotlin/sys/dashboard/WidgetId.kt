package sys.dashboard

import java.util.*

data class WidgetId(
    val rawValue: UUID = UUID.randomUUID()
)