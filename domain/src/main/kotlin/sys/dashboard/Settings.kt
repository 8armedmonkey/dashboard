package sys.dashboard

import kotlin.reflect.KClass

interface Settings {

    fun getKeys(): Set<String>

    fun containsKey(key: String): Boolean

    fun <T : Any> getValue(key: String, klass: KClass<T>): T?

    fun setValue(key: String, value: Any)

    fun remove(key: String)

    fun clear()

    fun isEmpty(): Boolean

}

inline fun <reified T : Any> Settings.getValue(key: String): T? = getValue(key, T::class)