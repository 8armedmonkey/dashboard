package sys.dashboard

interface SettingsFactory {

    fun getDefaultSettings(widgetId: WidgetId): Settings

}