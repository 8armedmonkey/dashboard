package sys.dashboard

import java.util.*

data class LayoutId(
    val rawValue: UUID = UUID.randomUUID()
)