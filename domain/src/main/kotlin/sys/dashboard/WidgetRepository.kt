package sys.dashboard

interface WidgetRepository {

    fun list(): List<Widget>

    fun store(widget: Widget)

    fun retrieve(widgetId: WidgetId): Widget?

    fun remove(widgetId: WidgetId)

    fun clear()

}