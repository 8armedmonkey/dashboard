package sys.dashboard

import java.util.*

data class WidgetAssignmentId(
    val rawValue: UUID = UUID.randomUUID()
)