package sys.dashboard

interface DashboardRepository {

    fun list(dashboardOwnerId: DashboardOwnerId): List<Dashboard>

    fun store(dashboardOwnerId: DashboardOwnerId, dashboard: Dashboard)

    fun retrieve(dashboardOwnerId: DashboardOwnerId, dashboardId: DashboardId): Dashboard?

    fun remove(dashboardOwnerId: DashboardOwnerId, dashboardId: DashboardId)

    fun clear(dashboardOwnerId: DashboardOwnerId)

}