package sys.dashboard

import java.util.*

data class AreaId(
    val rawValue: UUID = UUID.randomUUID()
)