package sys.dashboard

import kotlin.reflect.KClass
import kotlin.reflect.cast

abstract class BaseSettings : Settings {

    private val entries: MutableMap<String, Any> = mutableMapOf()

    override fun getKeys(): Set<String> {
        return entries.keys
    }

    override fun containsKey(key: String): Boolean {
        return entries.containsKey(key)
    }

    @ExperimentalStdlibApi
    override fun <T : Any> getValue(key: String, klass: KClass<T>): T? {
        return entries[key]?.let {
            if (klass.isInstance(it)) {
                klass.cast(it)
            } else {
                null
            }
        }
    }

    override fun setValue(key: String, value: Any) {
        entries[key] = value
    }

    override fun remove(key: String) {
        entries.remove(key)
    }

    override fun clear() {
        entries.clear()
    }

    override fun isEmpty(): Boolean {
        return entries.isEmpty()
    }

}