package sys.dashboard

data class LayoutMetaData(
    val name: String,
    val description: String
)