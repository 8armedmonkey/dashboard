package sys.dashboard

data class Widget(
    val id: WidgetId,
    val metaData: WidgetMetaData,
    val widgetClassName: String
)