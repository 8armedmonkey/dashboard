package sys.dashboard

import sys.utils.swap
import kotlin.math.max
import kotlin.math.min

class WidgetAssignments(
    val layout: Layout
) {

    private val entries: MutableMap<AreaId, MutableList<WidgetAssignment>> = mutableMapOf()

    fun getByArea(areaId: AreaId): List<WidgetAssignment> {
        assertLayoutContainsAreaId(areaId)
        return entries[areaId] ?: listOf()
    }

    fun isEmpty(): Boolean {
        return entries.isEmpty()
    }

    internal fun add(widget: Widget, areaId: AreaId, settings: Settings): WidgetAssignmentId {
        assertLayoutContainsAreaId(areaId)

        val widgetAssignment = WidgetAssignment(WidgetAssignmentId(), widget, settings)
        val entriesByArea = entries[areaId] ?: mutableListOf()

        entries[areaId] = mutableListOf(*entriesByArea.toTypedArray(), widgetAssignment)

        return widgetAssignment.id
    }

    internal fun configure(widgetAssignmentId: WidgetAssignmentId, settings: Settings) {
        entries.values.forEach { widgetAssignmentsByArea ->
            val widgetAssignment = widgetAssignmentsByArea.find { it.id == widgetAssignmentId }

            if (widgetAssignment != null) {
                val index = widgetAssignmentsByArea.indexOf(widgetAssignment)
                widgetAssignmentsByArea[index] = widgetAssignment.copy(settings = settings)
                return
            }
        }
        throw IllegalArgumentException("Widget assignment with the specified ID does not exist.")
    }

    internal fun increaseOrder(widgetAssignmentId: WidgetAssignmentId) {
        entries.values.forEach { widgetAssignmentsByArea ->
            val widgetAssignment = widgetAssignmentsByArea.find { it.id == widgetAssignmentId }

            if (widgetAssignment != null) {
                val index = widgetAssignmentsByArea.indexOf(widgetAssignment)
                val nextIndex = min(index + 1, widgetAssignmentsByArea.size - 1)
                widgetAssignmentsByArea.swap(index, nextIndex)
                return
            }
        }
        throw IllegalArgumentException("Widget assignment with the specified ID does not exist.")
    }

    internal fun decreaseOrder(widgetAssignmentId: WidgetAssignmentId) {
        entries.values.forEach { widgetAssignmentsByArea ->
            val widgetAssignment = widgetAssignmentsByArea.find { it.id == widgetAssignmentId }

            if (widgetAssignment != null) {
                val index = widgetAssignmentsByArea.indexOf(widgetAssignment)
                val previousIndex = max(index - 1, 0)
                widgetAssignmentsByArea.swap(index, previousIndex)
                return
            }
        }
        throw IllegalArgumentException("Widget assignment with the specified ID does not exist.")
    }

    internal fun moveToFirst(widgetAssignmentId: WidgetAssignmentId, areaId: AreaId) {
    }

    internal fun moveToLast(widgetAssignmentId: WidgetAssignmentId, areaId: AreaId) {
    }

    internal fun moveBefore(widgetAssignmentId: WidgetAssignmentId, before: WidgetAssignmentId) {
    }

    internal fun moveAfter(widgetAssignmentId: WidgetAssignmentId, after: WidgetAssignmentId) {
    }

    internal fun remove(widgetAssignmentId: WidgetAssignmentId) {
        entries.values.forEach { widgetAssignmentsByArea ->
            if (widgetAssignmentsByArea.removeIf { it.id == widgetAssignmentId }) {
                return
            }
        }
        throw IllegalArgumentException("Widget assignment with the specified ID does not exist.")
    }

    internal fun clear() {
        entries.clear()
    }

    private fun assertLayoutContainsAreaId(areaId: AreaId) {
        if (layout.hasArea(areaId).not()) {
            throw IllegalArgumentException("Layout does not contain the specified area.")
        }
    }

}
