package sys.utils

import org.junit.Assert.assertEquals
import org.junit.Test

class MutableListExtensionsTest {

    @Test
    fun `when swapped using different indices then the elements are swapped`() {
        val list = mutableListOf("foo", "bar", "baz")

        list.swap(0, 2)

        assertEquals("baz", list[0])
        assertEquals("bar", list[1])
        assertEquals("foo", list[2])
    }

    @Test
    fun `when swapped using same indices then the elements are not swapped`() {
        val list = mutableListOf("foo", "bar", "baz")

        list.swap(1, 1)

        assertEquals("foo", list[0])
        assertEquals("bar", list[1])
        assertEquals("baz", list[2])
    }

}