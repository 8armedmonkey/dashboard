package sys.dashboard

import org.junit.Assert.*
import org.junit.Test

class BaseSettingsTest {

    @Test
    fun `when entry is added then the key should be available`() {
        val settings = FakeSettings.newFakeBaseSettings()
        val key = "key"
        val value = "value"

        settings.setValue(key, value)

        assertTrue(settings.containsKey(key))
    }

    @Test
    fun `when entry is removed then the key should not be available`() {
        val settings = FakeSettings.newFakeBaseSettings()
        val key = "key"
        val value = "value"

        settings.setValue(key, value)
        settings.remove(key)

        assertFalse(settings.containsKey(key))
    }

    @Test
    fun `when entry is added then the value should be available`() {
        val settings = FakeSettings.newFakeBaseSettings()
        val key = "key"
        val value = "value"

        settings.setValue(key, value)

        assertEquals(value, settings.getValue<String>(key))
    }

    @Test
    fun `when entry is removed then the value should not be available`() {
        val settings = FakeSettings.newFakeBaseSettings()
        val key = "key"
        val value = "value"

        settings.setValue(key, value)
        settings.remove(key)

        assertNull(settings.getValue<String>(key))
    }

    @Test
    fun `when contains entry then it should not indicate as empty`() {
        val settings = FakeSettings.newFakeBaseSettings()
        val key = "key"
        val value = "value"

        settings.setValue(key, value)

        assertFalse(settings.isEmpty())
    }

    @Test
    fun `when contains no entry then it should indicate as empty`() {
        val settings = FakeSettings.newFakeBaseSettings()

        assertTrue(settings.isEmpty())
    }

    @Test
    fun `when cleared then it should remove all entries`() {
        val settings = FakeSettings.newFakeBaseSettings()
        val key = "key"
        val value = "value"

        settings.setValue(key, value)
        settings.clear()

        assertTrue(settings.isEmpty())
    }

}