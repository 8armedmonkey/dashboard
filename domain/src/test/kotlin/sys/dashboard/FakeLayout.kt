package sys.dashboard

import sys.utils.toNameUUID

object FakeLayout {

    fun newFakeSingleColumnLayout(): Layout = Layout(
        id = LayoutId("single-column-layout".toNameUUID()),
        metaData = LayoutMetaData(
            name = "Single-column Layout",
            description = "Layout with a single column."
        ),
        areaIds = setOf(
            AreaId("single-column".toNameUUID())
        )
    )

    fun newFakeThreeColumnLayout(): Layout = Layout(
        id = LayoutId("three-column-layout".toNameUUID()),
        metaData = LayoutMetaData(
            name = "Three-column Layout",
            description = "Layout with three columns."
        ),
        areaIds = setOf(
            AreaId("left-column".toNameUUID()),
            AreaId("middle-column".toNameUUID()),
            AreaId("right-column".toNameUUID())
        )
    )

}