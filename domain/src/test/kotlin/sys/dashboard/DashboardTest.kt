package sys.dashboard

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import sys.utils.toNameUUID
import java.util.*

class DashboardTest {

    private lateinit var dashboard: Dashboard

    @Before
    fun setUp() {
        dashboard = Dashboard(
            id = DashboardId("dashboard".toNameUUID()),
            metaData = DashboardMetaData(
                name = "Test Dashboard",
                description = "Dashboard under test"
            )
        )
    }

    @Test
    fun `when layout has not been assigned then it should not have any widget assignments`() {
        val widgetAssignments = dashboard.getWidgetAssignments()

        assertNull(widgetAssignments)
    }

    @Test
    fun `when layout has just been assigned then it should have empty widget assignments`() {
        val singleColumnLayout = FakeLayout.newFakeSingleColumnLayout()

        dashboard.setLayout(singleColumnLayout)

        val widgetAssignments = dashboard.getWidgetAssignments()

        assertNotNull(widgetAssignments)
        assertEquals(singleColumnLayout, widgetAssignments?.layout)
        assertEquals(true, widgetAssignments?.isEmpty())
    }

    @Test
    fun `when widget is added then it should be added to the correct area`() {
        val threeColumnLayout = FakeLayout.newFakeThreeColumnLayout()
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = threeColumnLayout.areaIds.elementAt(1)
        val settings = FakeSettings.newFakeSettings()

        dashboard.setLayout(threeColumnLayout)

        val widgetAssignmentId = dashboard.addWidget(widget, areaId, settings)
        val widgetAssignments = dashboard.getWidgetAssignments()
        val widgetAssignmentsByArea = widgetAssignments?.getByArea(areaId)
        val widgetAssignment = widgetAssignmentsByArea?.find { it.id == widgetAssignmentId }

        assertNotNull(widgetAssignment)
        assertEquals(widgetAssignmentId, widgetAssignment?.id)
        assertEquals(widget, widgetAssignment?.widget)
        assertEquals(settings, widgetAssignment?.settings)
    }

    @Test
    fun `when layout is re-assigned then it should clear any previous widget assignments`() {
        val singleColumnLayout = FakeLayout.newFakeSingleColumnLayout()
        val threeColumnLayout = FakeLayout.newFakeThreeColumnLayout()
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = singleColumnLayout.areaIds.first()
        val settings = FakeSettings.newFakeSettings()

        dashboard.setLayout(singleColumnLayout)
        dashboard.addWidget(widget, areaId, settings)
        dashboard.setLayout(threeColumnLayout)

        val widgetAssignments = dashboard.getWidgetAssignments()

        assertNotNull(widgetAssignments)
        assertEquals(threeColumnLayout, widgetAssignments?.layout)
        assertEquals(true, widgetAssignments?.isEmpty())
    }

    @Test
    fun `when widget is configured then it should be updated with the specified settings`() {
        val threeColumnLayout = FakeLayout.newFakeThreeColumnLayout()
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = threeColumnLayout.areaIds.elementAt(1)
        val settings = FakeSettings.newFakeSettings()
        val newSettings = FakeSettings.newFakeSettings()

        dashboard.setLayout(threeColumnLayout)

        val widgetAssignmentId = dashboard.addWidget(widget, areaId, settings)

        dashboard.configureWidget(widgetAssignmentId, newSettings)

        val widgetAssignments = dashboard.getWidgetAssignments()
        val widgetAssignmentsByArea = widgetAssignments?.getByArea(areaId)
        val widgetAssignment = widgetAssignmentsByArea?.find { it.id == widgetAssignmentId }

        assertEquals(newSettings, widgetAssignment?.settings)
    }

    @Test
    fun `when widget's order is increased then it should be moved to later position`() {
        val threeColumnLayout = FakeLayout.newFakeThreeColumnLayout()
        val widget1 = Widget(
            id = WidgetId("widget-1".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 1",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val widget2 = Widget(
            id = WidgetId("widget-2".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 2",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget2"
        )
        val areaId = threeColumnLayout.areaIds.elementAt(1)
        val settings = FakeSettings.newFakeSettings()

        dashboard.setLayout(threeColumnLayout)

        val widgetAssignmentId1 = dashboard.addWidget(widget1, areaId, settings)
        val widgetAssignmentId2 = dashboard.addWidget(widget2, areaId, settings)

        dashboard.increaseOrder(widgetAssignmentId1)

        val widgetAssignments = dashboard.getWidgetAssignments()
        val widgetAssignmentsByArea = widgetAssignments?.getByArea(areaId)

        assertEquals(widgetAssignmentId2, widgetAssignmentsByArea?.get(0)?.id)
        assertEquals(widget2, widgetAssignmentsByArea?.get(0)?.widget)

        assertEquals(widgetAssignmentId1, widgetAssignmentsByArea?.get(1)?.id)
        assertEquals(widget1, widgetAssignmentsByArea?.get(1)?.widget)
    }

    @Test
    fun `when widget's order is decreased then it should be moved to earlier position`() {
        val threeColumnLayout = FakeLayout.newFakeThreeColumnLayout()
        val widget1 = Widget(
            id = WidgetId("widget-1".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 1",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val widget2 = Widget(
            id = WidgetId("widget-2".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 2",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget2"
        )
        val areaId = threeColumnLayout.areaIds.elementAt(1)
        val settings = FakeSettings.newFakeSettings()

        dashboard.setLayout(threeColumnLayout)

        val widgetAssignmentId1 = dashboard.addWidget(widget1, areaId, settings)
        val widgetAssignmentId2 = dashboard.addWidget(widget2, areaId, settings)

        dashboard.decreaseOrder(widgetAssignmentId2)

        val widgetAssignments = dashboard.getWidgetAssignments()
        val widgetAssignmentsByArea = widgetAssignments?.getByArea(areaId)

        assertEquals(widgetAssignmentId2, widgetAssignmentsByArea?.get(0)?.id)
        assertEquals(widget2, widgetAssignmentsByArea?.get(0)?.widget)

        assertEquals(widgetAssignmentId1, widgetAssignmentsByArea?.get(1)?.id)
        assertEquals(widget1, widgetAssignmentsByArea?.get(1)?.widget)
    }

    @Test
    fun `when widget is moved before another widget in the same area then it should be moved to the correct position`() {
        val threeColumnLayout = FakeLayout.newFakeThreeColumnLayout()
        val widget1 = Widget(
            id = WidgetId("widget-1".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 1",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val widget2 = Widget(
            id = WidgetId("widget-2".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 2",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget2"
        )
        val widget3 = Widget(
            id = WidgetId("widget-3".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 3",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget3"
        )
        val areaId = threeColumnLayout.areaIds.elementAt(1)

        dashboard.setLayout(threeColumnLayout)

        val widgetAssignmentId1 = dashboard.addWidget(widget1, areaId, FakeSettings.newFakeSettings())
        val widgetAssignmentId2 = dashboard.addWidget(widget2, areaId, FakeSettings.newFakeSettings())
        val widgetAssignmentId3 = dashboard.addWidget(widget3, areaId, FakeSettings.newFakeSettings())

        dashboard.moveBefore(widgetAssignmentId3, before = widgetAssignmentId1)

        val widgetAssignments = dashboard.getWidgetAssignments()
        val widgetAssignmentsByArea = widgetAssignments?.getByArea(areaId)

        assertEquals(widgetAssignmentId3, widgetAssignmentsByArea?.get(0)?.id)
        assertEquals(widget3, widgetAssignmentsByArea?.get(0)?.widget)

        assertEquals(widgetAssignmentId1, widgetAssignmentsByArea?.get(1)?.id)
        assertEquals(widget1, widgetAssignmentsByArea?.get(1)?.widget)

        assertEquals(widgetAssignmentId2, widgetAssignmentsByArea?.get(2)?.id)
        assertEquals(widget2, widgetAssignmentsByArea?.get(2)?.widget)
    }

    @Test
    fun `when widget is moved before another widget in different area then it should be moved to the correct position`() {
        val threeColumnLayout = FakeLayout.newFakeThreeColumnLayout()
        val widget1 = Widget(
            id = WidgetId("widget-1".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 1",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val widget2 = Widget(
            id = WidgetId("widget-2".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 2",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget2"
        )
        val widget3 = Widget(
            id = WidgetId("widget-3".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 3",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget3"
        )
        val areaId1 = threeColumnLayout.areaIds.elementAt(0)
        val areaId2 = threeColumnLayout.areaIds.elementAt(1)

        dashboard.setLayout(threeColumnLayout)

        val widgetAssignmentId1 = dashboard.addWidget(widget1, areaId1, FakeSettings.newFakeSettings())
        val widgetAssignmentId2 = dashboard.addWidget(widget2, areaId1, FakeSettings.newFakeSettings())
        val widgetAssignmentId3 = dashboard.addWidget(widget3, areaId2, FakeSettings.newFakeSettings())

        dashboard.moveBefore(widgetAssignmentId1, before = widgetAssignmentId3)

        val widgetAssignments = dashboard.getWidgetAssignments()
        val widgetAssignmentsOnArea1 = widgetAssignments?.getByArea(areaId1)
        val widgetAssignmentsOnArea2 = widgetAssignments?.getByArea(areaId2)

        assertEquals(widgetAssignmentId2, widgetAssignmentsOnArea1?.get(0)?.id)
        assertEquals(widget2, widgetAssignmentsOnArea1?.get(0)?.widget)

        assertEquals(widgetAssignmentId1, widgetAssignmentsOnArea2?.get(0)?.id)
        assertEquals(widget1, widgetAssignmentsOnArea2?.get(0)?.widget)

        assertEquals(widgetAssignmentId3, widgetAssignmentsOnArea2?.get(1)?.id)
        assertEquals(widget3, widgetAssignmentsOnArea2?.get(1)?.widget)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when widget is moved before invalid widget then it should throw exception`() {
        val threeColumnLayout = FakeLayout.newFakeThreeColumnLayout()
        val widget1 = Widget(
            id = WidgetId("widget-1".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 1",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val widget2 = Widget(
            id = WidgetId("widget-2".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 2",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget2"
        )
        val areaId = threeColumnLayout.areaIds.elementAt(1)

        dashboard.setLayout(threeColumnLayout)

        val widgetAssignmentId1 = dashboard.addWidget(widget1, areaId, FakeSettings.newFakeSettings())
        dashboard.addWidget(widget2, areaId, FakeSettings.newFakeSettings())

        dashboard.moveBefore(widgetAssignmentId1, before = WidgetAssignmentId(UUID.randomUUID()))
    }

    @Test
    fun `when widget is removed then it should be removed from the area`() {
        val threeColumnLayout = FakeLayout.newFakeThreeColumnLayout()
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = threeColumnLayout.areaIds.elementAt(1)
        val settings = FakeSettings.newFakeSettings()

        dashboard.setLayout(threeColumnLayout)

        val widgetAssignmentId = dashboard.addWidget(widget, areaId, settings)

        dashboard.removeWidget(widgetAssignmentId)

        val widgetAssignments = dashboard.getWidgetAssignments()
        val widgetAssignmentsByArea = widgetAssignments?.getByArea(areaId)
        val widgetAssignment = widgetAssignmentsByArea?.find { it.id == widgetAssignmentId }

        assertNull(widgetAssignment)
    }

    @Test
    fun `when cleared then it should remove all widgets`() {
        val threeColumnLayout = FakeLayout.newFakeThreeColumnLayout()
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = threeColumnLayout.areaIds.elementAt(1)
        val settings = FakeSettings.newFakeSettings()

        dashboard.setLayout(threeColumnLayout)
        dashboard.addWidget(widget, areaId, settings)
        dashboard.clearWidgets()

        assertEquals(true, dashboard.getWidgetAssignments()?.isEmpty())
    }

}