package sys.dashboard

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import sys.utils.toNameUUID
import java.util.*

class WidgetAssignmentsTest {

    private lateinit var widgetAssignments: WidgetAssignments

    @Before
    fun setUp() {
        widgetAssignments = WidgetAssignments(FakeLayout.newFakeThreeColumnLayout())
    }

    @Test
    fun `when widget is added to a valid area then it should be added to the area`() {
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = widgetAssignments.layout.areaIds.first()
        val settings = FakeSettings.newFakeSettings()

        val widgetAssignmentId = widgetAssignments.add(widget, areaId, settings)

        val widgetAssignmentsByArea = widgetAssignments.getByArea(areaId)
        val widgetAssignment = widgetAssignmentsByArea.find { it.id == widgetAssignmentId }

        assertNotNull(widgetAssignment)
        assertEquals(widgetAssignmentId, widgetAssignment?.id)
        assertEquals(widget, widgetAssignment?.widget)
        assertEquals(settings, widgetAssignment?.settings)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when widget is added to an invalid area then it should throw exception`() {
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = AreaId(UUID.randomUUID())
        val settings = FakeSettings.newFakeSettings()

        widgetAssignments.add(widget, areaId, settings)
    }

    @Test
    fun `when widget is configured using valid reference then it should be successfully configured`() {
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = widgetAssignments.layout.areaIds.first()
        val settings = FakeSettings.newFakeSettings()
        val newSettings = FakeSettings.newFakeSettings()

        val widgetAssignmentId = widgetAssignments.add(widget, areaId, settings)

        widgetAssignments.configure(widgetAssignmentId, newSettings)

        val widgetAssignmentsByArea = widgetAssignments.getByArea(areaId)
        val widgetAssignment = widgetAssignmentsByArea.find { it.id == widgetAssignmentId }

        assertEquals(newSettings, widgetAssignment?.settings)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when widget is configured using invalid reference then it should throw exception`() {
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = widgetAssignments.layout.areaIds.first()
        val settings = FakeSettings.newFakeSettings()
        val newSettings = FakeSettings.newFakeSettings()

        widgetAssignments.add(widget, areaId, settings)

        widgetAssignments.configure(WidgetAssignmentId(UUID.randomUUID()), newSettings)
    }

    @Test
    fun `when widget is not in the last position and its order is increased then it should be moved to later position`() {
        val widget1 = Widget(
            id = WidgetId("widget-1".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 1",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val widget2 = Widget(
            id = WidgetId("widget-2".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 2",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val areaId = widgetAssignments.layout.areaIds.first()

        val widgetAssignmentId1 = widgetAssignments.add(widget1, areaId, FakeSettings.newFakeSettings())
        val widgetAssignmentId2 = widgetAssignments.add(widget2, areaId, FakeSettings.newFakeSettings())

        widgetAssignments.increaseOrder(widgetAssignmentId1)

        val widgetAssignmentsByArea = widgetAssignments.getByArea(areaId)

        assertEquals(widgetAssignmentId2, widgetAssignmentsByArea[0].id)
        assertEquals(widget2, widgetAssignmentsByArea[0].widget)

        assertEquals(widgetAssignmentId1, widgetAssignmentsByArea[1].id)
        assertEquals(widget1, widgetAssignmentsByArea[1].widget)
    }

    @Test
    fun `when widget is in the last position and its order is increased then it should remain in its position`() {
        val widget1 = Widget(
            id = WidgetId("widget-1".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 1",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val widget2 = Widget(
            id = WidgetId("widget-2".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 2",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val areaId = widgetAssignments.layout.areaIds.first()

        val widgetAssignmentId1 = widgetAssignments.add(widget1, areaId, FakeSettings.newFakeSettings())
        val widgetAssignmentId2 = widgetAssignments.add(widget2, areaId, FakeSettings.newFakeSettings())

        widgetAssignments.increaseOrder(widgetAssignmentId2)

        val widgetAssignmentsByArea = widgetAssignments.getByArea(areaId)

        assertEquals(widgetAssignmentId1, widgetAssignmentsByArea[0].id)
        assertEquals(widget1, widgetAssignmentsByArea[0].widget)

        assertEquals(widgetAssignmentId2, widgetAssignmentsByArea[1].id)
        assertEquals(widget2, widgetAssignmentsByArea[1].widget)
    }

    @Test
    fun `when widget is not in the first position and its order is decreased then it should be moved to earlier position`() {
        val widget1 = Widget(
            id = WidgetId("widget-1".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 1",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val widget2 = Widget(
            id = WidgetId("widget-2".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 2",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val areaId = widgetAssignments.layout.areaIds.first()

        val widgetAssignmentId1 = widgetAssignments.add(widget1, areaId, FakeSettings.newFakeSettings())
        val widgetAssignmentId2 = widgetAssignments.add(widget2, areaId, FakeSettings.newFakeSettings())

        widgetAssignments.decreaseOrder(widgetAssignmentId2)

        val widgetAssignmentsByArea = widgetAssignments.getByArea(areaId)

        assertEquals(widgetAssignmentId2, widgetAssignmentsByArea[0].id)
        assertEquals(widget2, widgetAssignmentsByArea[0].widget)

        assertEquals(widgetAssignmentId1, widgetAssignmentsByArea[1].id)
        assertEquals(widget1, widgetAssignmentsByArea[1].widget)
    }

    @Test
    fun `when widget is in the first position and its order is decreased then it should remain in its position`() {
        val widget1 = Widget(
            id = WidgetId("widget-1".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 1",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val widget2 = Widget(
            id = WidgetId("widget-2".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget 2",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget1"
        )
        val areaId = widgetAssignments.layout.areaIds.first()

        val widgetAssignmentId1 = widgetAssignments.add(widget1, areaId, FakeSettings.newFakeSettings())
        val widgetAssignmentId2 = widgetAssignments.add(widget2, areaId, FakeSettings.newFakeSettings())

        widgetAssignments.decreaseOrder(widgetAssignmentId1)

        val widgetAssignmentsByArea = widgetAssignments.getByArea(areaId)

        assertEquals(widgetAssignmentId1, widgetAssignmentsByArea[0].id)
        assertEquals(widget1, widgetAssignmentsByArea[0].widget)

        assertEquals(widgetAssignmentId2, widgetAssignmentsByArea[1].id)
        assertEquals(widget2, widgetAssignmentsByArea[1].widget)
    }

    @Test
    fun `when widget is removed using valid reference then it should be removed from the area`() {
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = widgetAssignments.layout.areaIds.first()
        val settings = FakeSettings.newFakeSettings()
        val widgetAssignmentId = widgetAssignments.add(widget, areaId, settings)

        widgetAssignments.remove(widgetAssignmentId)

        val widgetAssignmentsByArea = widgetAssignments.getByArea(areaId)
        val widgetAssignment = widgetAssignmentsByArea.find { it.id == widgetAssignmentId }

        assertNull(widgetAssignment)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when widget is removed using invalid reference then it should throw exception`() {
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = widgetAssignments.layout.areaIds.first()
        val settings = FakeSettings.newFakeSettings()

        widgetAssignments.add(widget, areaId, settings)
        widgetAssignments.remove(WidgetAssignmentId(UUID.randomUUID()))
    }

    @Test
    fun `when contains widget then it should not indicate as empty`() {
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = widgetAssignments.layout.areaIds.first()
        val settings = FakeSettings.newFakeSettings()

        widgetAssignments.add(widget, areaId, settings)

        assertFalse(widgetAssignments.isEmpty())
    }

    @Test
    fun `when contains no widget then it should indicate as empty`() {
        assertTrue(widgetAssignments.isEmpty())
    }

    @Test
    fun `when cleared then all widgets should be removed`() {
        val widget = Widget(
            id = WidgetId("widget".toNameUUID()),
            metaData = WidgetMetaData(
                name = "Test Widget",
                description = "Widget under test."
            ),
            widgetClassName = "sys.dashboard.widget.Widget"
        )
        val areaId = widgetAssignments.layout.areaIds.first()
        val settings = FakeSettings.newFakeSettings()

        widgetAssignments.add(widget, areaId, settings)
        widgetAssignments.clear()

        assertTrue(widgetAssignments.isEmpty())
    }

}